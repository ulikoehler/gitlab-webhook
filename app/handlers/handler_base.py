# Copyright (C) 2023 Andrew Lutsenko <anlutsenko@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may find one here:
# https://www.gnu.org/licenses/gpl-3.0.html
# or you may search the http://www.gnu.org website for the version 3 license,
# or you may write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA


from gitlab import Gitlab


class HandlerBase(object):
    def __init__(self, gitlab: Gitlab):
        self.gitlab = gitlab

    def handle(self, event: str, payload: dict[str, any]) -> bool:
        """Handle the webhook event

        :param str event: webhook event type
        :param dict[str, any] payload: webhook payload
        :return bool: True if event handling chain should stop
        """
        # Override this in child classes
        return False
