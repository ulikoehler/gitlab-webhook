from unittest.mock import MagicMock
from unittest import TestCase
from gitlab import Gitlab
from gitlab.v4 import objects


class HandlerTestBase(TestCase):
    def setUp(self):
        # base objects
        self.gitlab = MagicMock(Gitlab)
        self.project = MagicMock(objects.projects.Project)
        self.issue = MagicMock(objects.issues.ProjectIssue)
        self.mr = MagicMock(objects.merge_requests.ProjectMergeRequest)
        self.snippet = MagicMock(objects.snippets.ProjectSnippet)
        self.issue_note = MagicMock(objects.notes.ProjectIssueNote)
        self.mr_note = MagicMock(objects.notes.ProjectMergeRequestNote)
        self.snippet_note = MagicMock(objects.notes.ProjectSnippetNote)

        # manager objects
        self.gitlab.projects = MagicMock(objects.projects.ProjectManager)
        self.project.issues = MagicMock(objects.issues.ProjectIssueManager)
        self.project.mergerequests = MagicMock(objects.merge_requests.ProjectMergeRequestManager)
        self.project.snippets = MagicMock(objects.snippets.ProjectSnippetManager)
        self.issue.notes = MagicMock(objects.notes.ProjectIssueNoteManager)
        self.mr.notes = MagicMock(objects.notes.ProjectMergeRequestNoteManager)
        self.snippet.notes = MagicMock(objects.notes.ProjectSnippetNoteManager)

        # object getters
        self.gitlab.projects.get.return_value = self.project
        self.project.issues.get.return_value = self.issue
        self.project.mergerequests.get.return_value = self.mr
        self.project.snippets.get.return_value = self.snippet
        self.issue.notes.get.return_value = self.issue_note
        self.mr.notes.get.return_value = self.mr_note
        self.snippet.notes.get.return_value = self.snippet_note

        # basic attributes
        self.issue.description = "test issue"
        self.issue.labels = []
        self.issue.milestone = ""
