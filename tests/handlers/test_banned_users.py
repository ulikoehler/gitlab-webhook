# Copyright (C) 2023 Andrew Lutsenko <anlutsenko@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may find one here:
# https://www.gnu.org/licenses/gpl-3.0.html
# or you may search the http://www.gnu.org website for the version 3 license,
# or you may write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA


from app.handlers import BannedUsersHandler
from unittest.mock import patch

from .handler_test_base import HandlerTestBase


@patch("app.handlers.banned_users.BANNED_USERS", new=["banned1", "banned2"])
class TestBannedUsersHandler(HandlerTestBase):
    def testHandleIssue_OpenBanned(self):
        handler = BannedUsersHandler(self.gitlab)
        payload = {
            "user": {
                "username": "Banned2",
            },
            "project": {
                "id": 123,
            },
            "object_attributes": {
                "iid": 12345,
                "action": "open",
            },
        }
        assert handler.handle("Issue Hook", payload)
        self.issue.delete.assert_called_once_with()

    def testHandleIssue_OpenNotBanned(self):
        handler = BannedUsersHandler(self.gitlab)
        payload = {
            "user": {
                "username": "batman",
            },
            "project": {
                "id": 123,
            },
            "object_attributes": {
                "iid": 12345,
                "action": "open",
            },
        }
        assert not handler.handle("Issue Hook", payload)
        self.issue.delete.assert_not_called()

    def testHandleIssue_UpdateBanned(self):
        handler = BannedUsersHandler(self.gitlab)
        payload = {
            "user": {
                "username": "Banned2",
            },
            "project": {
                "id": 123,
            },
            "object_attributes": {
                "iid": 12345,
                "action": "update",
            },
        }
        assert not handler.handle("Issue Hook", payload)
        self.issue.delete.assert_not_called()

    def testHandleNote_issueBanned(self):
        handler = BannedUsersHandler(self.gitlab)
        payload = {
            "user": {
                "username": "Banned1"
            },
            "project": {
                "id": 123,
            },
            "object_attributes": {
                "id": 12345,
            },
            "issue": {
                "iid": 54321,
            },
        }
        assert handler.handle("Note Hook", payload)
        self.issue_note.delete.assert_called_once_with()
        self.mr_note.delete.assert_not_called()
        self.snippet_note.delete.assert_not_called()

    def testHandleNote_mrBanned(self):
        handler = BannedUsersHandler(self.gitlab)
        payload = {
            "user": {
                "username": "Banned1"
            },
            "project": {
                "id": 123,
            },
            "object_attributes": {
                "id": 12345,
            },
            "merge_request": {
                "iid": 54321,
            },
        }
        assert handler.handle("Note Hook", payload)
        self.issue_note.delete.assert_not_called()
        self.mr_note.delete.assert_called_once_with()
        self.snippet_note.delete.assert_not_called()

    def testHandleNote_snippetBanned(self):
        handler = BannedUsersHandler(self.gitlab)
        payload = {
            "user": {
                "username": "Banned1"
            },
            "project": {
                "id": 123,
            },
            "object_attributes": {
                "id": 12345,
            },
            "snippet": {
                "iid": 54321,
            },
        }
        assert handler.handle("Note Hook", payload)
        self.issue_note.delete.assert_not_called()
        self.mr_note.delete.assert_not_called()
        self.snippet_note.delete.assert_called_once_with()

    def testHandleNote_notBanned(self):
        handler = BannedUsersHandler(self.gitlab)
        payload = {
            "user": {
                "username": "batman"
            },
            "project": {
                "id": 123,
            },
            "object_attributes": {
                "id": 12345,
            },
            "issue": {
                "iid": 54321,
            },
            "merge_request": {
                "iid": 54321,
            },
            "snippet": {
                "iid": 54321,
            },
        }
        assert not handler.handle("Note Hook", payload)
        self.issue_note.delete.assert_not_called()
        self.mr_note.delete.assert_not_called()
        self.snippet_note.delete.assert_not_called()

    def testHandle_otherEvent(self):
        handler = BannedUsersHandler(self.gitlab)
        payload = {
            "user": {
                "username": "Banned2",
            },
            "project": {
                "id": 123,
            },
            "object_attributes": {
                "iid": 12345,
                "action": "open",
            },
        }
        assert not handler.handle("something else", payload)
